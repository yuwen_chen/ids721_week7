use anyhow::Result;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use serde_json::json;
use std::convert::TryInto;
use qdrant_client::qdrant::{
    CreateCollection, PointStruct, SearchPoints, VectorParams, VectorsConfig
};

async fn initial_vector(client: &QdrantClient, collection: &str) -> Result<()> {
    // Create 10 vectors
    let point = vec![
        PointStruct::new(
            1,
            vec![0.0, 10.0, 50.0], // Convert integers to floats
            json!({"name": "Author1"}).try_into().unwrap(),
        ),
        PointStruct::new(
            2,
            vec![25.0, 35.0, 100.0], // Convert integers to floats
            json!({"name": "Author2"}).try_into().unwrap(),
        ),
    ];

    // Upsert points into the collection
    client.upsert_points_blocking(collection, None, point, None).await.unwrap();

    Ok(())
}

async fn create(client: &QdrantClient, collection: &str) -> Result<()> {
    // Delete collection if it already exists
    let _ = client.delete_collection(collection).await;

    // Create collection with desired configuration
    client.create_collection(&CreateCollection {
        collection_name: collection.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 3,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}

async fn query(client: &QdrantClient, collection: &str) -> Result<()> {
    // Run a query on the data
    let search = client
        .search_points(&SearchPoints {
            collection_name: collection.into(),
            vector: vec![50.0, 50.0, 50.0],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    // Print header
    println!("The results:"); 
    for result in search.result.iter() {
        println!("{:?}\n", result); // Print each result on a new line
    }
    println!("Done query!"); 
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let client = initialize().await?;

    let collection = "collection";
    create(&client, collection).await?;
    initial_vector(&client, collection).await?;
    query(&client, collection).await?;

    Ok(())
}

async fn initialize() -> Result<QdrantClient> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    Ok(client)
}

